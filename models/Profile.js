const mongoose = require('mongoose');

const ProfileSchema = new mongoose.Schema({
    user:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'user'
    },

    company: {
        type: String
    },

    website: {
        type: String
    },
    location: {
        type: String
    },

    status: {
        type: String,
        required:true
    },

    skills: {
        type: [String],
        required: true
    },
    bio:{
        type: String
    },

    githubusername:{
        type: String
    },

    annonce: [
        {
            titre: {
                type: String,
                required: true
            },
            prix: {
                type: String,
                required: true
            },
            status: {
                type: String,
                required: true
            },
            typeannonce: {
                type: String,
                required:true
            },
            bedrooms: {
                type: String
            },
            bathrooms: {
                type: String
                
            },
            surface: {
                type: String,
                required:true
               
            },
            
            location: {
                type: String,
                required: true
               
            },
            pictures: [
                {

                img: String
                
                 }
        ]
        }
    ],
    

    
    social:{
        youtube: {
            type: String  
        },
        twitter: {
            type: String  
        },
        facebook: {
            type: String  
        },
        linkedin: {
            type: String  
        },
        instagram: {
            type: String  
        }
    },
    date:{
        type: Date,
        default: Date.now
    }
});

module.exports = Profile = mongoose.model('profile', ProfileSchema);