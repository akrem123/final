import React from 'react';
import {Route, Switch } from 'react-router-dom';
import Register from '../auth/Register';
import Login from '../auth/Login';
import AboutUs from "../layout/AboutUs";
import Agences from "../layout/Agences";
import Achat from "../layout/Achat";
import Location from "../layout/Location";
import Contact from "../layout/Contact";
import Alert from '../layout/Alert';
import Dashboard from '../dashboard/Dashboard';
import CreateProfile from '../profile-forms/CreateProfile';
import EditProfile from '../profile-forms/EditProfile';

import AddAnnonce from '../profile-forms/AddAnnonce';
import Profiles from '../profiles/Profiles';
import Profile from '../profile/Profile';
import PrivateRoute from '../routing/PrivateRoute';
import NotFound from '../layout/NotFound';

 const Routes = () => {
    return (
        <section className='container'>
              <Alert />
              <Switch>
                <Route exact path="/location/" component={Location} />
                <Route exact path="/Achat/" component={Achat} />
                <Route exact path="/Agences/" component={Agences} />
                <Route exact path="/Contact/" component={Contact} />
                <Route exact path="/AboutUs/" component={AboutUs} />
                <Route exact path="/register" component={Register} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/profiles" component={Profiles} />
                <Route exact path="/profile/:id" component={Profile} />
                <PrivateRoute exact path="/dashboard" component={Dashboard} />
                <PrivateRoute exact path="/create-profile" component={CreateProfile} />
                <PrivateRoute exact path="/edit-profile" component={EditProfile} />
                
                <PrivateRoute exact path="/add-annonce" component={AddAnnonce} />
                <Route component={NotFound} />
              </Switch>
            </section>
    );
};

export default Routes;