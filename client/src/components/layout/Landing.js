import React from "react";
import Hero from "./Hero";
import Banner from './Banner';
import { Link, Redirect } from "react-router-dom";
import Title from './Title'
import img2 from './images/room-2.jpeg'
import { connect } from 'react-redux';
import PropTypes from 'prop-types';






const Landing =({ isAuthenticated }) => {
  if (isAuthenticated) {
    return <Redirect to='dashboard' />
    
  }
  
    return (
      <>
          


       <Hero>
           <Banner 
                title="Découvrir Nos Agences " 
                subtitle="Deluxe Biens à Louer et à Vendre">
                <Link to="/Agences/" className="btn-primary">
                    Nos Agences
                </Link>
           </Banner>
        </Hero>



        <section className="tendances">
               <Title title="Rechercher "/>
               <div className="tendances-center">
               </div>
                  
        </section>

        

        <section className="tendances">
               <Title title="Trouver votre agence "/>
               <div className="tendances-center">
               </div>
                      
        </section>



            
          

        <section className="tendances">
               <Title title="Top Tendances ANNONCES IMMOBILIERES"/>
               <div className="tendances-center">
               </div>
                  <div>
                  <section className="roomslist">

<div className="roomslist-center">

    <article className="room">
        <div className="img-container">
                
             <img src={img2} alt="single room"></img>
             <div className="price-top">
                    <h6>TND
                        100
                    </h6>
                   
             </div>
            

               <Link to="/Agences/" className="btn-primary room-link">
               features
               </Link>
             
             </div>
        
        <p className="room-info">Appartement
    </p>
    </article>
                  

</div>

</section>
                  </div>
        </section>
        <section className="tendances">
               <Title title="DERNIÈRES ANNONCES IMMOBILIERES"/>
               <div className="tendances-center">
               </div>
                  <div>
                       Hello world
                  </div>
        </section>

      
        
        <section className="tendances">
               <Title title="Top Tendances ANNONCES IMMOBILIERES"/>
               <div className="tendances-center">
               </div>
                  <div>
                       Hello world
                  </div>
        </section>

       
        <section className="tendances">
               <Title title="Nos Offres"/>
               <div className="tendances-center">
               </div>
                  <div>
                       Hello world
                  </div>
        </section>
            
            
        </>
    );
  
};


Landing.propTypes = {
  isAuthenticated: PropTypes.bool
};

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated
}); 


export default connect(mapStateToProps)(Landing);