import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { deleteAnnonce} from '../../actions/profile';

const Annonce = ({ annonce, deleteAnnonce }) => {
    const annonces = annonce.map(ann => (
        <tr key={ann.id}>
            <td>{ann.titre}</td>
            <td className='hide-sm'>{ann.prix}</td>
            <td className='hide-sm'>{ann.status}</td>
            <td className='hide-sm'>{ann.typeannonce}</td>
            <td className='hide-sm'>{ann.bedrooms}</td>
            <td className='hide-sm'>{ann.bathrooms}</td>
            <td className='hide-sm'>{ann.surface}</td>
            <td className='hide-sm'>{ann.location}</td>
                        {/* <td className='hide-sm'>{ann.pictures}</td> */}
            <td>
                <button onClick={() => deleteAnnonce(ann._id)} className='btn btn-danger'>Delete</button>
            </td>
        </tr>
    ));
    return (
        <Fragment>
            <h2 className="my-2">Annonce Credentials</h2>
            <table className="table">
                <thead>
                    <tr>
                        <th>Titre Bien</th>
                        <th className='hide-sm'>Prix</th>
                        <th className='hide-sm'>Status</th>
                        <th className='hide-sm'>Type Annonce</th>
                        <th className='hide-sm'>Bedrooms</th>
                        <th className='hide-sm'>Bathrooms</th>
                        <th className='hide-sm'>Surface</th>
                        <th className='hide-sm'>Location</th>
                        {/* <th className='hide-sm'>Pictures</th> */}
                        <th />
                    </tr>
                </thead>
                <tbody>{annonces}</tbody>
            </table>
        </Fragment>
    );
};

Annonce.propTypes = {
    annonce: PropTypes.array.isRequired,
    deleteAnnonce: PropTypes.func.isRequired
};

export default connect(null, {deleteAnnonce})(Annonce);
