import React from 'react';
import PropTypes from 'prop-types';


const ProfileAnnonce =({
    annonce: {titre, status, location, typeannonce, prix}
}) => (
        <div>
            <h3 className="text-dark">{titre}</h3>
            
            <p>
                <strong>Type Annonce: </strong> {typeannonce}
            </p>
            <p>
                <strong>Status: </strong> {status}
            </p>
            <p>
                <strong>Prix: </strong> {prix}
            </p>
            <p>
                <strong>location: </strong> {location}
            </p>
        </div>
);

ProfileAnnonce.propTypes = {
    experience: PropTypes.array.isRequired
};

export default ProfileAnnonce;
