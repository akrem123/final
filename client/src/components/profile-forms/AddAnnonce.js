import React , { Fragment , useState }from 'react';
import { Link , withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addAnnonce } from '../../actions/profile';


const AddAnnonce = ({ addAnnonce, history }) => {
    const [formData, setFormData] = useState({
        titre: '',
        prix: '',
        status: '',
        typeannonce: '',
        bedrooms: '',
        bathrooms: '',
        surface: '',
        location: '',
        
    });

    

    const {titre, prix, status, typeannonce, bedrooms, bathrooms, surface, location } = formData;

    const onChange = e => setFormData({ ...formData, [e.target.name]: e.target.value });

    return (
        <Fragment>
            <h1 class="large text-primary">
       Add An Annonce
      </h1>
      <p class="lead">
        <i class="fas fa-code-branch"></i> Add Annonces available in your agency
      </p>
      <small>* = required field</small>
      <form class="form" onSubmit={e => { 
          e.preventDefault(); 
          addAnnonce(formData, history);
          }} >
        <div class="form-group">
          <input type="text" placeholder="* Titre" name="titre" value={titre} onChange={e=> onChange(e)} required />
        </div>
        <div class="form-group">
          <input type="text" placeholder="* Prix" name="prix" value={prix} onChange={e=> onChange(e)} required />
        </div>
        <div class="form-group">
        <select name="status" value={status} onChange={onChange}>
            <option value="0">* Select Property Status</option>
            <option value="Location">Location</option>
            <option value="Achat">Achat</option>
          </select>
        </div>
        <div class="form-group">
        <select name="typeannonce" value={typeannonce} onChange={onChange}>
            <option value="0">* Select Annonce Type</option>
            <option value="Appartement">Appartement</option>
            <option value="Villa">Villa</option>
            <option value="Terrain">Terrain</option>
          </select>
        </div>
        <div class="form-group">
          <input type="text" placeholder=" Bedrooms" name="bedrooms" value={bedrooms} onChange={e=> onChange(e)} />
        </div>
        <div class="form-group">
          <input type="text" placeholder=" Bathrooms" name="bathrooms" value={bathrooms} onChange={e=> onChange(e)} />
        </div>
        <div class="form-group">
          <input type="text" placeholder="* Surface" name="surface" value={surface} onChange={e=> onChange(e)} required />
        </div>
        <div class="form-group">
          <input type="text" placeholder="* Adresse" name="location" value={location} onChange={e=> onChange(e)} required />
        </div>
        
        <input type="submit" class="btn btn-primary my-1" />
        <Link class="btn btn-light my-1" to="/dashboard">Go Back</Link>
      </form>
        </Fragment>
    )
}

AddAnnonce.propTypes = {
    addAnnonce: PropTypes.func.isRequired

};

export default connect(null, { addAnnonce })(withRouter(AddAnnonce));
